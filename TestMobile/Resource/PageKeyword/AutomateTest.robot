*** Settings ***
Resource          ../../../KeywordRedefine/KeywordCommon/PageKeyword/KeywordCommon.robot
Resource          ../PageVariable/KrungthaiNextVariables.robot
Resource          Resource/PageKeyword/AutomateTestCommon.robot
*** Keywords ***
Switch To Context KrungthaiNext
    [Arguments]    ${TimetoStartMainPage}
    [Documentation]    *Sleep for wait Mainpage app for is switch to context webview*
    Sleep    ${TimetoStartMainPage}
    Switch To Context    WEBVIEW_ktbcs.netbankUAT    #WEBVIEW_unknown

Switch To Context Native
    [Documentation]    *Sleep for wait Mainpage app for is switch to context native*
    Switch To Context    NATIVE_APP    #NATIVE

Check Component in VerifyPin With ForgetPin
    [Documentation]    This Keyword Check Component in VerifyPin Witch ForgetPin
    Wait Mobile Until Page Contains Element    ${lblHeadForgetPIN}
    Mobile Page Should Contain Element    ${lblHeadForgetPIN}
    Mobile Page Should Contain Element    ${PIN1}
    Mobile Page Should Contain Element    ${PIN2}
    Mobile Page Should Contain Element    ${PIN3}
    Mobile Page Should Contain Element    ${PIN4}
    Mobile Page Should Contain Element    ${PIN5}
    Mobile Page Should Contain Element    ${PIN6}
    Mobile Page Should Contain Element    ${PIN7}
    Mobile Page Should Contain Element    ${PIN8}
    Mobile Page Should Contain Element    ${PIN9}
    Mobile Page Should Contain Element    ${PIN0}
    Mobile Page Should Contain Element    ${ForgetPIN}

Check Component in VerifyPin With ForgetPin EN
    [Documentation]    This Keyword Check Component in VerifyPin Witch ForgetPin
    Wait Mobile Until Page Contains Element    ${lblTitleEnterPIN}
    Mobile Page Should Contain Element    ${lblTitleEnterPIN}
    Mobile Page Should Contain Element    ${PIN1}
    Mobile Page Should Contain Element    ${PIN2}
    Mobile Page Should Contain Element    ${PIN3}
    Mobile Page Should Contain Element    ${PIN4}
    Mobile Page Should Contain Element    ${PIN5}
    Mobile Page Should Contain Element    ${PIN6}
    Mobile Page Should Contain Element    ${PIN7}
    Mobile Page Should Contain Element    ${PIN8}
    Mobile Page Should Contain Element    ${PIN9}
    Mobile Page Should Contain Element    ${PIN0}
    Mobile Page Should Contain Element    ${ForgetPIN}

Check Component in VerifyPin No ForgetPin
    [Documentation]    This Keyword Check Component in VerifyPin No ForgetPin
    Sleep    1
    Wait Mobile Until Page Contains Element    ${lblHeadForgetPIN}
    Mobile Page Should Contain Element    ${lblHeadForgetPIN}
    Mobile Page Should Contain Element    ${PIN1}
    Mobile Page Should Contain Element    ${PIN2}
    Mobile Page Should Contain Element    ${PIN3}
    Mobile Page Should Contain Element    ${PIN4}
    Mobile Page Should Contain Element    ${PIN5}
    Mobile Page Should Contain Element    ${PIN6}
    Mobile Page Should Contain Element    ${PIN7}
    Mobile Page Should Contain Element    ${PIN8}
    Mobile Page Should Contain Element    ${PIN9}
    Mobile Page Should Contain Element    ${PIN0}
    Capture ScreenShot

Check Component in VerifyPin No ForgetPin EN
    [Documentation]    This Keyword Check Component in VerifyPin No ForgetPin
    Sleep    1
    Wait Mobile Until Page Contains Element    ${lblTitleEnterPIN}
    Mobile Page Should Contain Element    ${lblTitleEnterPIN}
    Mobile Page Should Contain Element    ${PIN1}
    Mobile Page Should Contain Element    ${PIN2}
    Mobile Page Should Contain Element    ${PIN3}
    Mobile Page Should Contain Element    ${PIN4}
    Mobile Page Should Contain Element    ${PIN5}
    Mobile Page Should Contain Element    ${PIN6}
    Mobile Page Should Contain Element    ${PIN7}
    Mobile Page Should Contain Element    ${PIN8}
    Mobile Page Should Contain Element    ${PIN9}
    Mobile Page Should Contain Element    ${PIN0}
    Capture ScreenShot

KeyInputPin
    [Documentation]    This keyword for input pin "000000"
    sleep    2
    ${getPINButton}=    AppiumEx.Get Matching Xpath Count    //*[@id="btnPin0"]
    Run Keyword If    ${getPINButton} > 0
    ...    Run Keywords
    ...    Click Mobile Element    ${PIN0}
    ...    AND    Click Mobile Element    ${PIN0}
    ...    AND    Click Mobile Element    ${PIN0}
    ...    AND    Click Mobile Element    ${PIN0}
    ...    AND    Click Mobile Element    ${PIN0}
    ...    AND    Click Mobile Element    ${PIN0}

Common_VerifySessionTimeOut
    [Documentation]    This keyword check timeout application affter login via pin
    [Arguments]    ${getElementButton}
    sleep    3
    ${msgSectionTimeOutExist}=    AppiumEx.Get Matching Xpath Count    //*[@class="modal-desc text-center row"]    #//*[@class="modal-desc text-center row"]    #//*[@class="modal-desc text-center row"]//span[contains(text(),"ท่านไม่ได้ทำรายการในเวลาที่กำหนด กรุณา login เข้าสู่ระบบอีกครั้ง")]
    Comment    log    NumOfMsgSectionTimeOut : ${msgSectionTimeOutExist}
    Run Keyword If    ${msgSectionTimeOutExist} > 0    Common_LoginPIN    ${getElementButton}

Common_LoginPIN
    [Arguments]    ${getElementButton}
    Click Mobile Element    xpath=//*[@id="btnAcceptBaseModal"]
    sleep    3
    Click Mobile Element    ${getElementButton}
    KeyInputPin
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}

KrungThaiNextLogOut
    [Documentation]    This keyword for logout application krungthainext
    Click Mobile Element    ${btnSetting}
    Sleep    3
    Click Mobile Element    ${btnLogOut}
    sleep    3
    Click Mobile Element    ${btnOK_Alert}
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}

ContinueWarningDuplicateTransaction
    [Documentation]    This keyword check and click continue alert duplicate transaction
    Sleep    3
    Comment    ${Actual}=    Run Keyword And Ignore Error    Wait Mobile Until Page Contains Element    ${altDuplicateTransaction}
    Comment    Log    ${Actual}
    Comment    Run Keyword If    '@{Actual}[0]'=='PASS'    Click Mobile Element    ${btnAcceptBaseModal}
    ${Actual}=    AppiumEx.Get Matching Xpath Count    ${altDuplicateTransaction}
    Log    ${Actual}
    Run Keyword If    ${Actual} > 0    Click Mobile Element    ${btnAcceptBaseModal}
    Sleep    1
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    Sleep    1

ContinueWarningDuplicateTransaction_ENG
    [Documentation]    This keyword check and click continue alert duplicate transaction
    Sleep    3
    Comment    ${Actual}=    Run Keyword And Ignore Error    Wait Mobile Until Page Contains Element    ${altDuplicateTransaction_en}
    Comment    Log    ${Actual}
    Comment    Run Keyword If    '@{Actual}[0]'=='PASS'    Click Mobile Element    ${btnAcceptBaseModal}
    ${Actual}=    AppiumEx.Get Matching Xpath Count    ${altDuplicateTransaction_en}
    Log    ${Actual}
    Run Keyword If    ${Actual} > 0    Click Mobile Element    ${btnAcceptBaseModal}

ConvertAccountNo
    [Arguments]    ${accountno}
    ${To_AccountNo}    Convert To String    ${accountno}
    ${str_GetIndexToAccNo1}=    Get Substring    ${To_AccountNo}    6    9
    ${str_GetIndexToAccNo2}=    Get Substring    ${To_AccountNo}    9
    Set Suite Variable    ${str_GetToAccNo}    XXX-X-XX${str_GetIndexToAccNo1}-${str_GetIndexToAccNo2}

ConvertCardNo
    [Arguments]    ${cardno}
    ${strCardNo}    Convert To String    ${cardno}
    ${subStringCard1}=    Get Substring    ${strCardNo}    0    4
    ${subStringCard2}=    Get Substring    ${strCardNo}    4    6
    ${subStringCard3}=    Get Substring    ${strCardNo}    12
    Set Suite Variable    ${cvtCardNo}    ${subStringCard1} ${subStringCard2}XX XXXX ${subStringCard3}

GetOTPFromWeb
    [Documentation]    This keyword get otp number form web
    ${OTPRef}    Set Variable    ${NULL}
    ${OTPRef}=    Get mobile text    xpath=//*[contains(text(),"รหัสอ้างอิง")]
    Log to Console    ${OTPRef}
    ${OTPNumber}=    Get Substring    ${OTPRef}    12    17
    #Log to Console    OTP ${OTPNumber}
    Set Library Search Order    Selenium2Library
    Open Browser    http://10.9.213.76:21000/getTOP/GetTopValue?topReferance=${OTPNumber}    Chrome
    Sleep    5
    @{Window_list} =  Get Window Names
    ${Window1Title}=    Get Title
    Select Window   @{Window_list}[0]
    #Log To Console    @{Window_list}
    #Sleep    5
    ${GETOTPWEB}    Get Text    //body
    #Log To Console    ${GETOTPWEB}
    Set Suite Variable    ${GETOTPWEB}
    Sleep    5
    Close Browser

KrungthaiNextLaunchApplication
    Open Application Android    ${lo_IPAppium}    ${lo_PlatformName}    ${lo_PlatformVersion}    ${lo_SerialNumber}    ${KTBNextappPackage}    ${KTBNextappActivity}
    ...    ${lo_NoReset}    ${newCommandTimeout}    ${nativeWebScreenshot}
    Switch To Context KrungthaiNext    3
    #Wait Mobile Until Page Contains Element    xpath=//*[@src="assets/img/img_profile.png" or @src="assets/img/reciept_header.png"]
    AppiumEx.Wait Until Page Contains Element    xpath=//*[@src="assets/img/img_profile.png" or @src="assets/img/lg_splash.svg"]    60
    AppiumEx.Register Keyword To Run On Failure    Capture ScreenShot
    Sleep    3

ConvertMobileNumber
    [Arguments]    ${mobileno}
    ${MobileNumber}    Convert To String    ${mobileno}
    ${strMobileNo}=    Get Substring    ${MobileNumber}    6    10
    Set Suite Variable    ${strGetMobileNo}    xxx xxx ${strMobileNo}

SwitchUser
    [Documentation]    This keyword use for logout and login with new user (only use after open application)
    [Arguments]    ${username}    ${password}
    Sleep    1
    ${Actual}=    AppiumEx.Get Matching Xpath Count    //*[contains(text(),"บัญชีของฉัน")]
    Run Keyword If    ${Actual} > 0    ClickForgetPin
    Click Mobile Element    ${btnLoginWithUsername_Register}
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    Wait Mobile Until Page Does Not Contain Element    ${bkgDisableActivated_Register}
    Page Should Contain Element    ${lblTitleTermsAndCondition_Register}
    Page Should Contain Element    ${lblAcceptCheckBox_Register}
    Click Mobile Element    ${lblAcceptCheckBox_Register}
    Click Mobile Element    ${btnNextPage_Register} 
    Page Should Contain Element    ${lblTitleRegisterByUserID_Register}
    Page Should Contain Element    ${btnNext_Register}
    Sleep    2
    Input Mobile Text    ${txtInputUsername_Register}    ${username}
    Sleep    1
    Input Mobile Text    ${txtInputPassword_Register}    ${password}
    Sleep    2
    Click Mobile Element    ${lblTitleRegisterByUserID_Register}
    Click Mobile Element    ${btnNext_Register}
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    Page Should Contain Element    ${lblTitleConfirmMobileNo_Register}
    Page Should Contain Element    ${btnNextPage_Register}
    Click Mobile Element    ${btnNextPage_Register}
    Sleep    2
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    Wait Mobile Until Page Does Not Contain Element    ${bkgDisableActivated_Register}
    Page Should Contain Element    ${lblTitleConfirmOTP_Register}
    GetOTPFromWeb
    Set Library Search Order    AppiumEx
    Input Mobile Text    ${txtRegisterInputOTP}    ${GETOTPWEB}
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    Mobile Page Should Contain Element    ${lblRegisterSetPin}
    : FOR    ${INDEX}    IN RANGE    0    6
    \    Click Mobile Element    ${PIN0}
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    Sleep    2
    Mobile Page Should Contain Element    ${lblRegisterConfirmPin}
    : FOR    ${INDEX}    IN RANGE    0    6
    \    Click Mobile Element    ${PIN0}
    Sleep    5
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    Sleep    1

ClickForgetPin
    Click Mobile Element    ${btnMyAccount}
    Sleep    1
    Click Mobile Element    ${ForgetPIN}
    Sleep    1
    Click Mobile Element    ${btnOK_Alert}
    Sleep    1
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}

SwitchLanguageKrungthaiNext
    Set Library Search Order    AppiumEx
    KrungthaiNextLaunchApplication
    ${Actual}=    AppiumEx.Get Matching Xpath Count    //page-home//ion-footer//ion-col[1]//button
    Run Keyword If    ${Actual} > 0
    ...    Run Keywords
    ...    Click Mobile Element    xpath=//page-home//ion-footer//ion-col[1]//button
    ...    AND    Wait Mobile Until Element Visible    xpath=//page-authen-by-pin//ion-title//div
    ...    AND    KeyInputPin
    ...    AND    Sleep    3
    ...    AND    Common_VerifySessionTimeOut    xpath=//page-home//ion-footer//ion-col[1]//button
    ...    AND    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    ...    AND    Click Mobile Element    xpath=//*[@class="margin-top5 menu-bar"]//ion-icon[@class="icon icon-ios custom-setting-default"]
    ...    AND    Sleep    3
    ...    AND    Click Mobile Element    xpath=//page-setting-main//*[@class="scroll-content"]//ion-row[1]//setting-menu[5]
    ...    AND    Sleep    3
    ...    AND    Click Mobile Element    ${lblEditLanguage}
    ...    AND    Sleep    3
    ...    AND    Click Mobile Element    ${lblEnglishLanguage}
    ...    AND    Sleep    1
    ...    AND    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    ...    AND    Sleep    3
    ...    ELSE    Run Keywords
    ...    Click Mobile Element    xpath=//*[@class="setButton"]//div[@class="bold row box-change-lang"]/div[2]
    ...    AND    Sleep    1
    ...    AND    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    ...    AND    Sleep    3
    [Teardown]     Close Application

SwitchBackKrungthaiNext
    Set Library Search Order    AppiumEx
    KrungthaiNextLaunchApplication
    ${Actual}=    AppiumEx.Get Matching Xpath Count    //page-home//ion-footer//ion-col[1]//button
    Run Keyword If    ${Actual} > 0
    ...    Run Keywords
    ...    Click Mobile Element    xpath=//page-home//ion-footer//ion-col[1]//button
    ...    AND    Wait Mobile Until Element Visible    xpath=//page-authen-by-pin//ion-title//div
    ...    AND    KeyInputPin
    ...    AND    Sleep    3
    ...    AND    Common_VerifySessionTimeOut    xpath=//page-home//ion-footer//ion-col[1]//button
    ...    AND    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    ...    AND    Click Mobile Element    xpath=//*[@class="margin-top5 menu-bar"]//ion-icon[@class="icon icon-ios custom-setting-default"]
    ...    AND    Sleep    3
    ...    AND    Click Mobile Element    xpath=//page-setting-main//*[@class="scroll-content"]//ion-row[1]//setting-menu[5]
    ...    AND    Sleep    3
    ...    AND    Click Mobile Element    ${lblEditLanguage}
    ...    AND    Sleep    3
    ...    AND    Click Mobile Element    ${lblThaiLanguage}
    ...    AND    Sleep    1
    ...    AND    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    ...    AND    Sleep    3
    ...    ELSE    Run Keywords
    ...    Click Mobile Element    xpath=//*[@class="setButton"]//div[@class="bold row box-change-lang"]/div[1]
    ...    AND    Sleep    1
    ...    AND    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    ...    AND    Sleep    3
    [Teardown]     Close Application

ContinueWarningDuplicateTransactionEN
    [Documentation]    This keyword check and click continue alert duplicate transaction
    Sleep    3
    ${Actual}=    AppiumEx.Get Matching Xpath Count    ${altDuplicateTransactionEN}
    Log    ${Actual}
    Run Keyword If    ${Actual} > 0    Click Mobile Element    ${btnAcceptBaseModal}
    Sleep    1
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    Sleep    1

GetOTPFromWebEN
    [Documentation]    This keyword get otp number form web
    ${OTPRef}    Set Variable    ${NULL}
    ${OTPRef}=    Get mobile text    xpath=//*/span[contains(text(),"Ref")]
    Log to Console    ${OTPRef}
    ${OTPNumber}=    Get Substring    ${OTPRef}    4    10
    #Log to Console    OTP ${OTPNumber}
    Set Library Search Order    Selenium2Library
    Open Browser    http://10.9.213.76:21000/getTOP/GetTopValue?topReferance=${OTPNumber}    Chrome
    Sleep    5
    @{Window_list} =  Get Window Names
    ${Window1Title}=    Get Title
    Select Window   @{Window_list}[0]
    #Log To Console    @{Window_list}
    #Sleep    5
    ${GETOTPWEB}    Get Text    //body
    #Log To Console    ${GETOTPWEB}
    Set Suite Variable    ${GETOTPWEB}
    Sleep    5
    Close Browser

KrungThaiNextLogOutEN
    [Documentation]    This keyword for logout application krungthainext
    Click Mobile Element    ${btnSettingEN}
    Sleep    3
    Click Mobile Element    ${btnLogOut}
    sleep    3
    Click Mobile Element    ${btnOK_Alert}
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}

SwitchUserEN
    [Documentation]    This keyword use for logout and login with new user (only use after open application)
    [Arguments]    ${username}    ${password}
    Sleep    3
    ${Actual}=    AppiumEx.Get Matching Xpath Count    //*[contains(text(),"My Account")]
    Run Keyword If    ${Actual} > 0    ClickForgetPinEN
    Click Mobile Element    ${btnLoginWithUsernameEN}
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    Wait Mobile Until Page Does Not Contain Element    ${bkgDisableActivated_Register}
    Page Should Contain Element    ${lblTitleRegisterTermAndConditionEN}
    Click Mobile Element    ${chbRegisterAcceptCondition}
    Click Mobile Element    ${btnNextPage_Register} 
    Page Should Contain Element    ${lblAccessToKrungThaiNextEN}
    Page Should Contain Element    ${btnNext_Register}
    Sleep    2
    Input Mobile Text    ${txtInputUsername_RegisterEN}    ${username}
    Sleep    1
    Input Mobile Text    ${txtInputPassword_RegisterEN}    ${password}
    Sleep    2
    Click Mobile Element    ${lblAccessToKrungThaiNextEN}
    Click Mobile Element    ${btnNext_Register}
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    Page Should Contain Element    ${lblRegisterConfirmMobilePhoneEN}
    Page Should Contain Element    ${btnNextPage_Register}
    Click Mobile Element    ${btnNextPage_Register}
    Sleep    2
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    Wait Mobile Until Page Does Not Contain Element    ${bkgDisableActivated_Register}
    Page Should Contain Element    ${lblRegisterConfirmOTPEN}
    GetOTPFromWebEN
    Set Library Search Order    AppiumEx
    Input Mobile Text    ${txtRegisterInputOTP}    ${GETOTPWEB}
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    Mobile Page Should Contain Element    ${lblRegisterSetPinEN}
    : FOR    ${INDEX}    IN RANGE    0    6
    \    Click Mobile Element    ${PIN0}
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    Sleep    2
    Mobile Page Should Contain Element    ${lblRegisterConfirmPinEN}
    : FOR    ${INDEX}    IN RANGE    0    6
    \    Click Mobile Element    ${PIN0}
    Sleep    5
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}
    Sleep    1
    
ClickForgetPinEN
    Click Mobile Element    ${btnMyAccount_en}
    Sleep    1
    Click Mobile Element    ${ForgetPIN}
    Sleep    1
    Click Mobile Element    ${btnOK_Alert}
    Sleep    1
    Wait Mobile Until Page Does Not Contain Element    ${imgLoading}

Press Delete Key
    Press Keycode    67

Press Delete Keys
    [Arguments]    ${forTimes}
    : FOR    ${INDEX}    IN RANGE    1    ${forTimes}
    \    Press Keycode    67

ConvertMonthShortThaiToNumber
    [Arguments]    ${strMonth}
    ${monthNo}=    Set Variable If    
    ...    '${strMonth}' == 'ม.ค.'    01
    ...    '${strMonth}' == 'ก.พ.'    02
    ...    '${strMonth}' == 'มี.ค.'    03
    ...    '${strMonth}' == 'เม.ย.'    04
    ...    '${strMonth}' == 'พ.ค.'    05
    ...    '${strMonth}' == 'มิ.ย.'    06
    ...    '${strMonth}' == 'ก.ค.'    07
    ...    '${strMonth}' == 'ส.ค.'    08
    ...    '${strMonth}' == 'ก.ย.'    09
    ...    '${strMonth}' == 'ต.ค.'    10
    ...    '${strMonth}' == 'พ.ย.'    11
    ...    '${strMonth}' == 'ธ.ค.'    12
    [Return]    ${monthNo}

ConvertMonthNoToThaiShort
    [Arguments]    ${monthNo}
    ${thaiShort}=    Set Variable If    
    ...    '${monthNo}' == '01'    ม.ค.
    ...    '${monthNo}' == '02'    ก.พ.
    ...    '${monthNo}' == '03'    มี.ค.
    ...    '${monthNo}' == '04'    เม.ย.
    ...    '${monthNo}' == '05'    พ.ค.
    ...    '${monthNo}' == '06'    มิ.ย.
    ...    '${monthNo}' == '07'    ก.ค.
    ...    '${monthNo}' == '08'    ส.ค.
    ...    '${monthNo}' == '09'    ก.ย.
    ...    '${monthNo}' == '10'    ต.ค.
    ...    '${monthNo}' == '11'    พ.ย.
    ...    '${monthNo}' == '12'    ธ.ค.
    [Return]    ${thaiShort}

ConvertMonthNoToThaiLong
    [Arguments]    ${monthNo}
    ${ThaiLong}=    Set Variable If    
    ...    '${monthNo}' == '01'    มกราคม
    ...    '${monthNo}' == '02'    กุมภาพันธ์
    ...    '${monthNo}' == '03'    มีนาคม
    ...    '${monthNo}' == '04'    เมษายน
    ...    '${monthNo}' == '05'    พฤษภาคม
    ...    '${monthNo}' == '06'    มิถุนายน
    ...    '${monthNo}' == '07'    กรกฎาคม
    ...    '${monthNo}' == '08'    สิงหาคม
    ...    '${monthNo}' == '09'    กันยายน
    ...    '${monthNo}' == '10'    ตุลาคม
    ...    '${monthNo}' == '11'    พฤศจิกายน
    ...    '${monthNo}' == '12'    ธันวาคม
    [Return]    ${ThaiLong}

ConvertYearShortBEToCE
    [Arguments]    ${strYear}
    ${strYear}=    Set Variable    25${strYear}
    ${strYear}=    Convert To Integer    ${strYear}
    ${strCEYear}=    Evaluate    ${strYear}-543
    ${strCEYear}=    Convert To String    ${strCEYear}
    [Return]    ${strCEYear}

ConvertYearCEToShortBE
    [Arguments]    ${strYear}
    ${strYear}=    Convert To Integer    ${strYear}
    ${strShortBE}=    Evaluate    ${strYear}+543
    ${strShortBE}=    Convert To String    ${strShortBE}
    ${strShortBE}=    Get Substring    ${strShortBE}    2    4
    [Return]    ${strShortBE}

ConvertYearCEToBE
    [Arguments]    ${strYear}
    ${strYear}=    Convert To Integer    ${strYear}
    ${strBE}=    Evaluate    ${strYear}+543
    ${strBE}=    Convert To String    ${strBE}
    [Return]    ${strBE}